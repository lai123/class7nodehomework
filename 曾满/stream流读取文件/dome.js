// 作业1:使用open write close的形式去创建一个大文件,
//内容从指定的文本中随机取一些（每次取得长度内容可以不一样）,
//计算出这个大文件出现最多的字符(要使用流的形式).



let fs = require("fs");
let fils=fs.openSync("./filess/bigdata.txt","a");
let str="^_^^_^";
for (let i = 0; i < 100000; i++) {
    let starnums=Math.floor(Math.random()*str.length);
    let endlength=Math.ceil(Math.random()*str.length);
    let content=str.substr(starnums,endlength);

    fs.writeSync(fils,content);
    
}

var rs = fs.createReadStream('./filess/bigdata.txt', 'utf-8');
rs.on("data", (chunk) => {
    console.log("DATA:---------------------");
    console.log(chunk);
});
fs.closeSync(fils);


function maxs(content) {
    let obj = {}
    for (let j = 0; j < content.length; j++) {
        if (obj[content[j]] == undefined) {
            obj[content[j]] = 1;
        } else {
            obj[content[j]]++;
        }
    }

    let maxstring = null;
    let max = -Infinity;
    for (let key in obj) {
        if (obj[key] > max) {
            max = obj[key];
            maxstring = key;
        }
    }
    return maxstring;
}