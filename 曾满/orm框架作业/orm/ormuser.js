//引入配置的数据库连接


let conn=require("./ormdatabase");

//引入

const { Sequelize, Model, DataTypes } = require('sequelize');

//初始化对象
const sequelize = new Sequelize(conn.database, conn.user, conn.password, {
    //申明下使用的数据库
    dialect: 'mysql',
    host: conn.host,
    port: conn.port,
    logging: true,//日志
    timezone: '+08:00',//东八区
    define: {
        timestamps: false,
    }
});
//要不要创建表
sequelize.sync({ force: false })

//创建对象,构建模型
class User extends Model { }
User.init({
    // attributes
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    user_name: {
        type: DataTypes.STRING
        // allowNull defaults to true
    },
    user_password: {
        type: DataTypes.STRING
    },
    user_money: {
        type: DataTypes.STRING
    },
    login_date: {
        type: DataTypes.DATE
    }
}, {
    sequelize,
    modelName: 'user',
    // options
    tableName: 'user',//要关联表
});
//查询所有的数据
// User.findAll().then(users => {
//     console.log("All users:", JSON.stringify(users, null, 4));
// });
//属性，attributes 要查询哪些字段
// User.findAll({attributes: ['user_id', 'user_name']}).then(users => {
//     console.log("All users:", JSON.stringify(users, null, 4));
// });

//条件查询
User.findAll({
    where: {
        user_name: 'zss',
        user_password: 'zss@123456'
    }
}).then(users => {
    console.log("正确的输入:", JSON.stringify(users, null, 4));
});
//看看框架能不能实现防sql注入
User.findAll({
    where: {
        user_name: "aaa ' or 1=1 or '1",
        user_password: 'fsdfsd'
    }
}).then(users => {
    console.log("sql注入的结果", JSON.stringify(users, null, 4));
});

//查询单条数据
User.findOne({
    where: {
        user_id: 1
    }
}).then(user => {
    console.log("查询单条的数据" + JSON.stringify(user, null, 4))
})
//能不能使用异步
async function useAsync() {
    let users = await User.findAll();
    console.log("异步函数查询的结果:" + JSON.stringify(users, null, 4))
}
useAsync();


