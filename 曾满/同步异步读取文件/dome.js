//创建1000个文件，计算同步创建与异步创建所需的时间
let fs = require("fs");



//同步创建
let startime=new Date().getTime();
for (let i = 0; i < 1000; i++) {
   fs.writeFileSync(+i+".txt","内容");
    
}
let endtime=new Date().getTime();
console.log("同步时间:"+(endtime-startime));

//异步创建
let num=0;
let startime2=new Date().getTime();
for (let j = 0; j < 1000; j++) {
    fs.writeFile(j+".txt","内容2",(error)=>{
        num++;
        if(num==1000){
            let endtime2=new Date().getTime();
            console.log("异步时间:"+(endtime2-startime2));
        }
    });
     
 }