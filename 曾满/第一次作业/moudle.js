module.exports={
    //加
      add: function (a,b)   {
        if(this.error(a,b)){
            console.log('加法');
            return a+b;
        }else{
            console.log('加法输入错误');
        }
        
    },
    //减
    subtract: function (a,b){
        if(this.error(a,b)){
            console.log('减法');
            return a-b;
        }else{
            console.log('减法输入错误');
        }
       
    },

    //乘
    multiply: function (a,b){
        if(this.error(a,b)){
            console.log('乘法');
            return a*b;
        }else{
            console.log('乘法输入错误');
        }
      
    },

    //除
    divide: function (a,b){
        if(this.error(a,b)){

            if(b==0){
                console.log('除数为0，错误')
            }
            console.log('除法');
            return a/b;
        }else{
            console.log('除法输入错误');
        }
        
    },
    error: (a, b) => {
        if (isNaN(a) == true || isNaN(b) == true) {
            return false;
        } else {
            return true;
        }
    }

}