// ## 作业2:用同步异步分别生成1000个文件，分别同步的耗时异步的耗时 
let fs = require("fs");
if(!fs.existsSync("files")) fs.mkdirSync("files");
//同步
let time1 = new Date().getTime();
for (let i=0;i<1000;i++) {
    fs.writeFileSync("./files/"+i+".txt","test")
}
let time2 = new Date().getTime();
console.log("同步耗时："+(time2-time1));

//异步
let createNum = 0;
if(!fs.existsSync("files")) fs.mkdirSync("files");
let time3 = new Date().getTime();
for (let k=0;k<1000;k++) {
    fs.writeFile("./files/" + k + ".txt","test",(err) => {
        createNum++;
        if(createNum == 1000) {
            let time4 = new Date().getTime();
            console.log("异步耗时："+ (time4-time3));
        }
    })
}