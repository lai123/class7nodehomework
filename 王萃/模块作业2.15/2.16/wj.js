// ## 作业1：生成100个文件，每个文件存入一个1到1000的随机数字，再取出最大值的那个文件,值也要取出来,再取出最小的那个.

let fs = require("fs");
let num = [];
let max=0;
let min=1001;
let maxname = 0;
let minname = 0;

for (i=1;i<101;i++) {
    let random = Math.ceil(Math.random()*1000);
    fs.writeFileSync("./"+i+".txt",""+random+"");
    num[i] = (fs.readFileSync("./"+i+".txt").toString());
    //max
    if(Number(num[i]>max)) {
        max = Number(num[i]);
        maxname=i;
    }

    //min
    if(Number(num[i]<min)) {
        min = Number(num[i]);
        minname=i;
    }
}

console.log(num);
console.log("最大值的文件名是："+maxname+".txt，最大值为："+max);
console.log("最小值的文件名是："+minname+".txt，最大值为："+min);
