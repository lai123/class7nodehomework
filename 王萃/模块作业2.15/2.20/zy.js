// ## 作业1:使用open write close的形式去创建一个大文件,内容从指定的文本中随机取一些（每次取得长度内容可以不一样）,计算出这个大文件出现最多的字符(要使用流的形式).

//引入fs文件
let fs = require("fs");
let wj = fs.openSync("./wenjian.txt","w");

//随机定义字符串
let str = "hfdewhfwehfgoeruihfsh124321374hfgdfjngjkd";
for(i=0;i<100;i++) {
    let long = Math.ceil(Math.random() * 20);
    let kong = "";
    for(k=0;k<long;k++) {
        let sj = Math.ceil(Math.random() * str.length);
        kong = str.charAt(sj);
    }

    fs.writeFileSync(wj,kong,{flag:'a'});

}
//关闭文件
fs.closeSync(wj);

//计算出这个大文件出现最多的字符
let sz=[];
let read = fs.createReadStream("./wenjian.txt","utf-8");
read.on("data", (suiji) =>{
    console.log(suiji);
    let nr = suiji;
    for(let j=0;j<nr.length;j++) {
        if(sz[nr.charAt(j)] != undefined) {
            sz[nr.charAt(j)] = sz[nr.charAt(j)] +1;
        }
        else{
            sz[nr.charAt(j)] = 1;
        }
    }
    let max = 0;
    let zhi = "";
    for(let key in sz) {
        if(sz[key] > max) {
            max = sz[key];
            zhi=key;
        }
    }
    console.log(sz);
    console.log("这个文件出现最多的字符是：" +zhi+",共出现了"+max+"次");

});
//结束监听事件
read.on("end",()=>{
    console.log("-------------读取完毕----------------");
})


