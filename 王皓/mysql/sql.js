//实现分页,能实现男女个数和总数的统计,能统计出（小于18） 和 18-45 和 45岁以上的人数（可以的话一条sql,分组,如果不能行就写多个sql统计

let mysql = require("mysql");
let connection = mysql.createConnection({ host: "127.0.0.1", user: "root", password: "root", port: "3306", database: "user" });

connection.connect();

function sql(i){
    let sql1="select * from human limit "+((i-1)*2)+",2";
    connection.query(sql1,(err,data)=>{
    console.log(err);
    console.log(data);
})
}
sql(1);


let sql2="select sex,count(*) from human GROUP BY sex ";
connection.query(sql2,(err,data)=>{
    console.log(err);
    console.log(data);
})


let sql3 = "select COUNT(*) as '总数',(select count(*)  from human WHERE sex='男') as '男生个数',(select count(*) from human WHERE sex='女') as '女生个数',(select count(*)  from human where age<18) as '小于18岁',(select count(*) from human where age<45 and age>18)  as '18-45' ,(select count(*) from human where age>45)  as '大于45岁' from human";
connection.query(sql3, (err, data) => {
    console.log(err);
    console.log(data);

})



connection.end()