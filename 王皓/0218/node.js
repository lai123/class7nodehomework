let fs = require("fs");

if (!fs.existsSync("filesycn")) fs.mkdirSync("filesycn");

let syncStartTime = new Date().getTime();
for (let i = 0; i < 2000; i++) {
    fs.writeFileSync("./filesycn/" + i + ".txt", "sync");
}
let syncEndTime = new Date().getTime();
console.log("同步创建的时间:" + (syncEndTime - syncStartTime));

let createNum = 0;
if (!fs.existsSync("filesasync")) fs.mkdirSync("filesasync");
let asyncStartTime = new Date().getTime();
for (let j = 0; j < 2000; j++) {
    fs.writeFile("./filesasync/" + j + ".txt", "sync", (err) => {
        createNum++;
        if (createNum == 2000) {
            let asyncEndTime = new Date().getTime();
            console.log("异步创建的时间:" + (asyncEndTime - asyncStartTime));
        }
    })
}
