// 把sequelize 配置完成,至少能查询一个表去看看sequelize的文档
let config = require('./config.js');

const {Sequelize,Model,DataTypes} = require('sequelize');

const sequelize = new Sequelize(config.database,config.user,config.password,{
    dialect:'mysql',
    host:config.host,
    port:config.port,
    logging:true,
    timezone:'+08:00',
    define:{
        timestamps:false,
    }
})
//创建表取消
sequelize.sync({force:false})

class yyy extends Model{}
yyy.init({
    id:{
        type:DataTypes.INTEGER,
        allowNull:false,
        primaryKey: true
    },
    name:{
        type: DataTypes.STRING
    }
},{
    sequelize,
    modelName:'yyy',
    tableName:'yyy',
});
yyy.findAll().then(name =>{
    console.log(JSON.stringify(name,null,4));
})