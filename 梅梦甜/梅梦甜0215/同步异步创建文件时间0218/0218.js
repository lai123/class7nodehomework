let fs = require("fs");
//先创建一个文件夹
if (!fs.existsSync("syncfiles")) fs.mkdirSync("syncfiles");  //判断是否存在这个文件
//计算同步创建1000个文件的耗时
let syncStartTime = new Date().getTime(); //获取当前的时间 时间戳 单位毫秒
for (let i = 0; i < 1001; i++) {    //创建文件
    fs.writeFileSync("./syncfiles/" + i + ".txt", "thisissynctest");
}
let syncEndTime = new Date().getTime();  //获取结束的时间
console.log("同步创建的时间：" + (syncEndTime - syncStartTime));




//异步创建文件
let createNum = 0;   //创建的次数
if (!fs.existsSync("asyncfiles")) fs.mkdirSync("asyncfiles");
let asyncStartTime = new Date().getTime();  //获取当前的时间
for (let j = 0; j < 1001; j++) {
    fs.writeFile("./asyncfiles/" + j + ".txt", "thisissynctest", (err) => {
        createNum++;
        if (createNum == 1000) {
            let asyncEndTime = new Date().getTime();
            console.log("异步创建的时间：" + (asyncEndTime - asyncStartTime));
        }
    })
}



