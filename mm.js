// 作业：创建一个学生表,里面有班级字段,里面最好有自己班上同学姓名,
//       其它班级的姓名随机写,有个字段存储游戏的分值,先实现随机的数值写入,
//       查询出各班级前三名的学生

//引入配置文件
let zz = require('./zz');
//引入
const { Sequelize, Model, DataTypes, json } = require('sequelize');


//初始化对象
const sequelize = new Sequelize(zz.database, zz.user, zz.password, {
    //申明下使用的数据库
    dialect: 'mysql',
    host: zz.host,
    port: zz.port,
    loggin: false,//日志
    timezon: "+08:00",//东八区
    define: {
        timestamps: false,
        underscored: false,
        deleteAt: false

    }
});

 //创建对象，构建模型
class ttt extends Model{}
 ttt.init({
     user_id:{
       type:DataTypes.INTEGER,
               allowNull:false,
        primaryKey:true,
        autoIncrement:true,

     },
    name:{
       type:DataTypes.STRING
     },
    fs:{
         type:DataTypes.STRING
    },
     class:{
         type:DataTypes.STRING

}

},{
    sequelize,
    modelName:"ttt",
    tableName:"ttt",
 });

 async function useAsync(){
     ttt.sync();
let stu=await ttt.findAll();
    console.log("异步函数查询的结果:"+JSON.stringify(stu,null,4))
 }
useAsync();

 setTimeout(function(){
     let num1=Math.floor(Math.random()*101);
    let stu1 = new ttt({name:'新之助',fs:num1,class:7});
    stu1.save();

    let num2=Math.floor(Math.random()*101);
    let stu2=new ttt({name:'风间',fs:num2,class:7});
     stu2.save();

    let num3=Math.floor(math.random()*101);
    let stu3=new ttt({name:'正南',fs:num3,class:7});
    stu3.save();

 }
 ,3000)