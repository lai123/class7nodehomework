//## 作业2:生成1000个文件，用同步异步分别计算生成文件的时间.
let fs=require("fs");
//同步写入
var time1=new Date().getTime();

for(i=1;i<=1000;i++){
    fs.writeFileSync("./files/"+i+".txt","中午吃啥");
              
}
var time2=new Date().getTime();
console.log( "同步写入:"+(time2-time1)+"毫秒");

var time3=new Date().getTime();
//异步写入
for(i=1;i<=1000;i++){
  
    fs.writeFile("./files2/"+i+".txt","中午吃啥",function(err){
      
    });     
}
var time4=new Date().getTime();
console.log( "异步写入:"+(time4-time3)+"毫秒");