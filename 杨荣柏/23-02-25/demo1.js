// 能不能使用promise的异步函数,封装下数据库的查询
let mysql = require('mysql');
var cont = mysql.createConnection({host:'127.0.0.1',user:'root',password:'root',database:'y'})
cont.connect();
function query(sql){
    return new Promise((resolve,reject)=>{
        cont.query(sql,(err,data)=>{
            if(err!=null){
                reject(err)
            }else{
                resolve(data)
            }
        })
    })
}

async function useAsync(sql){
    let conts = await query(sql);
    console.log(conts);
    cont.end();
}


var sql = 'select * from yyy'
useAsync(sql)



