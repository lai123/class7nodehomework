//作业：写个加减乘除的模块（尽量少写暴露,要考虑复用性），供外部使用. 作业每个分支要建一个目录（自己名字）
module.exports = {
    plus: function (a, b) {
        return "加：" + (a + b);
    },
    minus: function (a, b) {
        return "减：" + (a - b);
    },
    multiply: function (a, b) {
        return "乘：" + (a * b);
    },
    divide: function (a, b) {
        if (b == 0) {
            return "被除数不能为0";
        }
        return "除：" + (a / b);
    }
}