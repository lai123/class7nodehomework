//生成100个文件，每个文件存入1到1000的随机数字，再取出最大的那个文件，值也要取出来，再取出最小的那个
let fs=require("fs");
for(let i=0;i<100;i++){
    let filename=i+".txt";
    //同步读取
    fs.writeFileSync(filename,Math.floor(Math.random()*1001)+"");
}
let maxNum=null;
let maxName="";
for(let j=0;j<100;j++){
    let filename=j+".txt";
    let content=fs.readFileSync(filename);
    content=content.toString();
    if(j==0){
        maxNum=Number(content);
        maxName=filename;
    }
    if(maxNum<Number(content)){
        maxNum=Number(content);
        maxName=filename;
    }
}
console.log(maxNum,maxName);
