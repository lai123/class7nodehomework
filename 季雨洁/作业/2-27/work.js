//作业：创建一个学生表,里面有班级字段,里面最好有自己班上同学姓名,其它班级的姓名随机写,
//有个字段存储游戏的分值,先实现随机的数值写入,查询出各班级前三名的学生
let mysql=require("mysql");
let con=mysql.createConnection({
    host:"localhost",
    user:"root",
    password:"root",
    port:"3306",
    database:"student"
});
con.connect();

let date=new Date();
let time=date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate()+"-"+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();

let score=Math.floor(Math.random()*101);
let clas=Math.ceil(Math.random()*3);

let insert="insert into student(`name`,`score`) values ('张三','99'),('李四','100'),('王武','1000'),('张丽','1000'),('曾满','10')"

// con.query(insert,(err,data)=>{
//     console.log(err);
//     console.log(data);
// })

let sel="select * from student ORDER BY score DESC LIMIT 3";
con.query(sel,(err,data)=>{
        console.log(err);
        console.log(data);
 })
 con.end();