//作业:第一个能不能使用promise的异步函数,封装下数据库的查询,
//二 把sequelize 配置完成,至少能查询一个表去看看sequelize的文档,实现curd
let mysql=require("mysql");
let con=mysql.createConnection({
    host:"localhost",
    user:"root",
    password:"root",
    port:"3306",
    database:"user"
});
con.connect();

function query(sql){
    return new Promise((resolve,reject)=>{
        con.query(sql,(err,data)=>{
            if(err!=null){
                reject(err)
            }else{
                resolve(data)
            }
        })
    })
}

async function useAsync(sql){
    let cons=await query(sql);
    console.log(cons);
}

useAsync('select * from user')
con.end();
