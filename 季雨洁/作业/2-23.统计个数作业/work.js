//作业：实现分页,能实现男女个数和总数的统计,能统计出（小于等于18） 和 
//18-45 和 45岁以上的人数（可以的话一条sql,分组,使用占位符的形式去写）

let mysql=require("mysql");
let con = mysql.createConnection({
    host: "localhost", 
    user: "root", 
    password: "root",
    port: "3306", 
    database: "user"
});
con.connect();

function way(){
    let waysql1="SELECT count(1) AS '<18' FROM `user`  where age<18 ";
    let waysql2="SELECT count(1) AS '>45' FROM `user`  where age>45 ";
    let waysql3='SELECT count(1) AS "18~45" FROM `user`  where age>18 and age<45 ';
    let waysql4='SELECT sex,count(1) as "数量",(SELECT COUNT(1) FROM `user`) as"总数" FROM `user` GROUP BY sex ;';
 

    con.query(waysql4, (err, data) => {
        console.log(err);
        console.log(data);
    });
}
way();

con.end()