//生成1000个文件，用同步和异步计算他们的时间差
//同步创建文件
let fs = require('fs');
if(!fs.existsSync("files"))fs.mkdirSync("files");
let syncStartTime=new Date().getTime();
for(let i=0;i<2000;i++){
    fs.writeFileSync("files"+i+".txt","123456");
}
let syncEndTime=new Date().getTime();
console.log("同步创建时间："+(syncEndTime-syncStartTime));

//异步创建文件
let createNum=0;
if(!fs.existsSync("afiles")) fs.mkdirSync("afiles");
let asyncStartTime=new Date().getTime();
for(let j=0;j<2000;j++){
    fs.writeFile("afiles"+j+".txt","123456",(err)=>{
        createNum++;
        if(createNum==2000){
            let asyncEndTime=new Date().getTime();
            console.log("异步创建时间："+(asyncEndTime-asyncStartTime));
        }
    })
}