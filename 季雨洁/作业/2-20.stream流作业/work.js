//作业1:使用open write close的形式去创建一个大文件,
//内容从指定的文本中随机取一些（每次取得长度内容可以不一样）,计算出这个大文件出现最多的字符(要使用流的形式).
let fs=require("fs");
let fa=fs.openSync("./files/","a");
let str="使用open write close的形式去创建一个大文件";
for(let i=0;i<3;i++){
    let snum=Math.floor(Math.random()*str.length);
    let sleng=Math.ceil(Math.random()*str.length);
    let content=str.substr(snum,sleng);
    fs.writeSync(fa,content);
}
console.log(fs.readFileSync("./files/").toString());
console.log("出现次数最多的字符："+way(fs.readFileSync("./files/").toString()));
fs.closeSync(fa);


function way(content){
    let obj={}
    for(let j=0;j<content.length;j++){
        if(obj[content[j]]==undefined){
            obj[content[j]]=1;
        }else{
            obj[content[j]]++;
        }
    }
    let maxstr=null;
    let max=0;
    for (let key in obj) {
        if (obj[key]>max) {
            max = obj[key];
            maxstr=key;
        }
    }
    return maxstr;
}