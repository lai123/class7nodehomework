// ### 作业：写个加减乘除的模块（尽量少写暴露,要考虑复用性）
// ，供外部使用. 作业每个分支要建一个目录（自己名字）
let obj={
    jia:function(a,b){
        return a+b;
    },
    jian:function(a,b){
       return a-b;
        
    },
    cheng:function(a,b){
        return a*b;
    },
    chu:function(a,b){
        if(a==0){
            return "错误";
        }else{
            return a/b;
        }
    }
}
module.exports = obj;
// 相当于对外暴露的空对象

