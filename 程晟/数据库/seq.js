const {Sequelize,Model,DataTypes}=require('sequelize');
let database = {
    host: "localhost",
    user: "root",
    password: "root",
    port: 3306,
    database: 'mydb'
}
const sequelize=new Sequelize(database.database,database.user,database.password,{
    dialect:'mysql',
    host:database.host,
    port:database.port,
    logging:true,
    timezone: '+08:00',
    define: {
        timestamps: false,
    }
});
sequelize.sync({ force: false })
class User extends Model { }
User.init({
    u_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    u_name: {
        type: DataTypes.STRING
       
    }
},{
    sequelize,
    modelName: 'user',
    
    tableName: 'user',
});
User.findAll().then(users => {
    console.log("All users:", JSON.stringify(users, null, 4));
});