/*
 Navicat Premium Data Transfer

 Source Server         : hhh
 Source Server Type    : MySQL
 Source Server Version : 50737
 Source Host           : 127.0.0.1:3306
 Source Schema         : hjh

 Target Server Type    : MySQL
 Target Server Version : 50737
 File Encoding         : 65001

 Date: 23/02/2023 17:33:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sex` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '小米', '女', 20);
INSERT INTO `user` VALUES (2, '小明', '男', 30);
INSERT INTO `user` VALUES (3, '小李', '男', 48);
INSERT INTO `user` VALUES (4, '小军', '男', 80);
INSERT INTO `user` VALUES (5, '小红', '女', 46);
INSERT INTO `user` VALUES (6, '李子云', '男', 88);
INSERT INTO `user` VALUES (7, '李小云', '女', 20);
INSERT INTO `user` VALUES (8, '李中云', '男', 66);
INSERT INTO `user` VALUES (9, '李大云', '男', 99);
INSERT INTO `user` VALUES (10, '李白云', '女', 22);
INSERT INTO `user` VALUES (11, '李乌云', '女', 38);
INSERT INTO `user` VALUES (12, '李彩云', '女', 25);
INSERT INTO `user` VALUES (13, '李母云', '女', 36);
INSERT INTO `user` VALUES (14, '李公云', '男', 50);
INSERT INTO `user` VALUES (15, '李云云', '女', 33);
INSERT INTO `user` VALUES (16, '小何', '男', 5);

SET FOREIGN_KEY_CHECKS = 1;
