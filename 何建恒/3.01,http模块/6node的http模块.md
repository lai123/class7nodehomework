
## http 协议

网站.小程序.app 基本都是基于http协议

Node.js开发的目的就是为了用JavaScript编写Web服务器程序。因为JavaScript实际上已经统治了浏览器端的脚本，其优势就是有世界上数量最多的前端开发人员。如果已经掌握了JavaScript前端开发，再学习一下如何将JavaScript应用在后端开发，就是名副其实的全栈了。

## HTTP协议

Web系统的基础就是HTTP协议,HTTP协议是一个应用层协议,也就是TCP传输层的上一层协议,HTTP协议只定义传输的内容是什么,不定义如何传输(这是底层协议做的事),所以理解HTTP协议,只需要理解协议的数据结构及所代表的意义即可。

tcp/ip协议,长连接

以 hellobike为例

电动车 《--------------------------》hellobike服务器（跟支付宝的服务有通信,可以立即为就是同一台）
 芯片                                     |
                                          |
                                        扫码

http协议

客户端网页 --------------------------》服务器

          《--------------------------close（数据给了以后就关闭）

           <--------------------------close


无连接:无连接的含义是限制每次连接只处理一个请求。服务器处理完客户的请求,并收到客户的应答后,即断开连接。采用这种方式可以节省传输时间。可以设置Connecction:keep-alive 保存TCP连接。后面如果还要用到这个连接不会每次都新建连接.


无状态:HTTP协议是无状态协议。无状态是指协议对于事务处理没有记忆能力。缺少状态意味着如果后续处理需要前面的信息,则它必须重传,这样可能导致每次连接传送的数据量増大。另一方面,在服务器不需要先前信息时它的应答就较快。

有状态就像有来电提醒,无状态就是没有来电提醒.

明文传输,HTTP协议不支持加密处理,所以在安全性方面是一大硬伤。目前解决这一安全问题的方法是使用Https协议(基于HTTP+SSL/TLS协议)的一种安全传输方案。


HTTP服务器

要开发HTTP服务器程序，从头处理TCP连接，解析HTTP是不现实的。这些工作实际上已经由Node.js自带的http模块完成了。应用程序并不直接和HTTP协议打交道，而是操作http模块提供的request和response对象。

request对象封装了HTTP请求，我们调用request对象的属性和方法就可以拿到所有HTTP请求的信息；

response对象封装了HTTP响应，我们操作response对象的方法，就可以把HTTP响应返回给浏览器。

用Node.js实现一个HTTP服务器程序非常简单。我们来实现一个最简单的Web程序hello.js，它对于所有请求，都返回Hello world!：


```js

'use strict';

// 导入http模块:
var http = require('http');

// 创建http server，并传入回调函数:
var server = http.createServer(function (request, response) {
    // 回调函数接收request和response对象,
    // 获得HTTP请求的method和url:
    console.log(request.method + ': ' + request.url);
    // 将HTTP响应200写入response, 同时设置Content-Type: text/html:
    response.writeHead(200, {'Content-Type': 'text/html'});
    // 将HTTP响应的HTML内容写入response:
    response.end('<h1>Hello world!</h1>');
});

// 让服务器监听8080端口:
server.listen(8080);

console.log('Server is running at http://127.0.0.1:8080/');


```

## 搭建一个web服务器,针对不同的参数，给予不同的回应,把html文件用异步方式去读取,返回.再去试下用orm框架去操作数据给不同参数返回不同的数据.

