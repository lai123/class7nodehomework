/*
 Navicat Premium Data Transfer

 Source Server         : hhhh
 Source Server Type    : MySQL
 Source Server Version : 50737
 Source Host           : 127.0.0.1:3306
 Source Schema         : hjh

 Target Server Type    : MySQL
 Target Server Version : 50737
 File Encoding         : 65001

 Date: 27/02/2023 17:14:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sore` int(100) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 7, '何建恒', 84, '2023-02-27 16:39:35', '2023-02-27 16:39:35');
INSERT INTO `user` VALUES (2, 7, '李彩云', 88, '2023-02-27 16:40:10', '2023-02-27 16:40:10');
INSERT INTO `user` VALUES (3, 7, '王大皓', 2, '2023-02-27 16:40:35', '2023-02-27 16:40:35');
INSERT INTO `user` VALUES (4, 7, '孙诗强', 28, '2023-02-27 16:41:01', '2023-02-27 16:41:01');
INSERT INTO `user` VALUES (5, 7, '吴阿杰', 68, '2023-02-27 16:41:24', '2023-02-27 16:41:24');
INSERT INTO `user` VALUES (6, 7, '王逸民儿', 100, '2023-02-27 16:42:03', '2023-02-27 16:42:03');
INSERT INTO `user` VALUES (7, 5, '小明', 89, '2023-02-27 16:42:31', '2023-02-27 16:42:31');
INSERT INTO `user` VALUES (8, 5, '小红', 83, '2023-02-27 16:42:52', '2023-02-27 16:42:52');
INSERT INTO `user` VALUES (9, 7, '罗虫子', 79, '2023-02-27 16:43:38', '2023-02-27 16:43:38');
INSERT INTO `user` VALUES (10, 5, '小刚', 36, '2023-02-27 16:44:09', '2023-02-27 16:44:09');
INSERT INTO `user` VALUES (11, 5, '小强', 56, '2023-02-27 16:44:24', '2023-02-27 16:44:24');
INSERT INTO `user` VALUES (12, 6, '小小', 51, '2023-02-27 16:46:03', '2023-02-27 16:46:03');
INSERT INTO `user` VALUES (13, 6, '中中', 69, '2023-02-27 16:57:35', '2023-02-27 16:57:35');
INSERT INTO `user` VALUES (14, 6, '大大', 33, '2023-02-27 16:57:42', '2023-02-27 16:57:42');
INSERT INTO `user` VALUES (15, 6, '胖胖', 68, '2023-02-27 16:58:26', '2023-02-27 16:58:26');
INSERT INTO `user` VALUES (16, 6, '瘦瘦', 39, '2023-02-27 16:58:35', '2023-02-27 16:58:35');
INSERT INTO `user` VALUES (17, 5, '流云', 15, '2023-02-27 17:00:14', '2023-02-27 17:00:14');
INSERT INTO `user` VALUES (18, 5, '流云', 71, '2023-02-27 17:10:32', '2023-02-27 17:10:32');
INSERT INTO `user` VALUES (19, 5, '流云', 71, '2023-02-27 17:10:32', '2023-02-27 17:10:32');
INSERT INTO `user` VALUES (20, 5, '流云', 9, '2023-02-27 17:10:45', '2023-02-27 17:10:45');

SET FOREIGN_KEY_CHECKS = 1;
