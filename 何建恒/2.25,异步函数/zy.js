// 作业:第一个能不能使用promise的异步函数,封装下数据库的查询,二 把sequelize 配置完成,至少能查询一个表去看看sequelize的文档
let fs=require('fs');
let mysql=require('mysql');
let h=mysql.createConnection({host:'127.0.0.1',user:'root',password:'root',port:'3306',database:"hjh"})
function  j(sql){
    // 有一个promise
    return new Promise ((resolve,reject)=>{
        h.query(sql,(err,data)=>{
            if(err!=null){
                reject(err);
            }else{
                resolve(data);
            }
        })
    })
}
async function hh(sql){
    let content = await j(sql);
    console.log(content);
}
hh("select * from user")
h.end();