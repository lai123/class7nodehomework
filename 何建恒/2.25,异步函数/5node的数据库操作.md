## 后端工作:curd  create update read delete



## 对mysql的操作

当我们安装好MySQL后，Node.js程序如何访问MySQL数据库呢？

访问MySQL数据库只有一种方法，就是通过网络发送SQL命令，然后，MySQL服务器执行后返回结果。

我们可以在命令行窗口输入mysql -u root -p，然后输入root口令后，就连接到了MySQL服务器。因为没有指定--host参数，所以我们连接到的是localhost，也就是本机的MySQL服务器。

在命令行窗口下，我们可以输入命令，操作MySQL服务器：

## 对mysql的直接操作

如果直接使用mysql包提供的接口，我们编写的代码就比较底层，例如，查询代码

```js

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '123456',
  database : 'test'
});
 
connection.connect();


```


## mysql 的curd

执行sql语句的方法

一 直接将要执行的sql语句放入

```js

connection.query(`delete from test where id= 1`, (error, results) => {
    if (error) throw error;
    console.log(results);
});

```


二 用?替换sql执行时需要更改的参数
将需要作为参数的部分用?代替，在后面传入参数作为?依次顺序对应的值，需要注意的是，?替换进去的字符串值会被默认加上' '，比如这个"测试问号"和2被替换进?后sql语句是这样的update test set name = '测试问号' where id = 2

```js

connection.query(`update test set name = ? where id = ?`, ["测试问号", 2], (error, results) => {
    if (error) throw error;
    console.log(results);
});

```

三 设置执行参数
sql执行时可以带超时参数，替换?值参数

```js

connection.query({
  sql: `update test set name = ? where id = ?`,
  timeout: 40000, // 40s
  values:  ["测试问号", 2]
}, function (error, results, fields) {
    if (error) throw error;
    console.log(results);
});

```

接着引用官方的连接实例来一个完整的增删查改：


```js
var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'example.org',
  user     : 'bob',
  password : 'secret'
});
 
connection.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }
 
  console.log('connected as id ' + connection.threadId);
});
// 增
connection.query(`insert into test(name) values(?)`,"测试",(error, results) =>{
    if (error) throw error;
    console.log(results);
});

// 删
connection.query(`delete from test where id= ?`,1,(error, results) =>{
    if (error) throw error;
    console.log(results);
});

// 查
connection.query('SELECT * from test', function (error, results, fields) {
    if (error) throw error;
    console.log("查询结果是");
    console.log(results);
});

// 改

connection.query(`update test set name = "测试修改" where id = 2`,(error, results) =>{
    if (error) throw error;
    console.log(results);
});

// 关闭连接
connection.end();

```





## ORM是什么？

ORM（Object Relational Mapping，对象关系映射），是一种为了解决面向对象与关系数据库存在的互不匹配的现象的技术，通过描述对象和数据库之间映射的元数据，把程序中的对象自动持久化到关系数据库中。它的作用是在关系型数据库和对象之间作一个映射，这样，我们在具体的操作数据库的时候，就不需要再去和复杂的SQL语句打交道，只要像平时操作对象一样操作它就可以了 。这就好比入java的Mybatis， thinkphp的model类，通过映射数据库，简化数据库操作，使开发者不需要书写复杂的SQL语句，将更多的时间放在逻辑的书写上。



Sequelize
这个被star数最多了一个ORM框架，官方居然不给中文文档，找个CLI命令快速构建也没有，也没找到个合适轮子，只能自己搭了，也不是少了轮子就不能活了。不过Sequelize的官网文档看着很顺眼，不得不称赞一下，需要注意的一点Sequelize v5版本发生了比较大的变化，这里我以最新版本v5版本为主，老版本可以自己看看下官方文档。Sequelize v5


### 安装

```js

$ npm install --save sequelize
$ npm install --save mysql2


```

数据库的配置文件config.js

```js

module.exports = {
​    database: {
​        dbName: 'TEST',
​        host: 'localhost',
​        port: 3306,
​        user: 'root',
​        password: '123456'
​    }
}



```


构建数据库访问公共文件db.js

```js

const Sequelize = require('sequelize')
const {
​    dbName,
​    host,
​    port,
​    user,
​    password
} = require('../config').database

const sequelize = new Sequelize(dbName, user, password, {
​    dialect: 'mysql',
​    host,
​    port,
​    logging: true,
​    timezone: '+08:00',
​    define: {
​        // create_time && update_time
​        timestamps: true,
​        // delete_time
​        paranoid: true,
​        createdAt: 'created_at',
​        updatedAt: 'updated_at',
​        deletedAt: 'deleted_at',
​        // 把驼峰命名转换为下划线
​        underscored: true,
        //作用域规则 可以包括与常规查找器 where, include, limit 等所有相同的属性
​        scopes: {
​            bh: {
​                attributes: {
​                    exclude: ['password', 'updated_at', 'deleted_at', 'created_at']
​                }
​            },
​            iv: {
​                attributes: {
​                    exclude: ['content', 'password', 'updated_at', 'deleted_at']
​                }
​            }
​        }
​    }
})
// 创建模型,Sequelizesync将创建不存在的表。如果您已经拥有所有表，则它不会执行任何操作。但是，如果您使用force: true，它将删除存在的表并从模型定义中重新创建它们
sequelize.sync({
​    force: false
})
module.exports = {
​    sequelize
}


```

构建模型

```js

const {Sequelize, Model} = require('sequelize')
const {db} = require('../../db')

class User extends Model {}
User.init({
    // attributes
    firstName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    lastName: {
        type: Sequelize.STRING
        // allowNull defaults to true
    }
}, {
    db,
    modelName: 'user'
    // options
});


```

CRUD操作：然后看一下逻辑层，就非常简单了，直接使用ES7 async/await即可

```js

// Find all users
User.findAll().then(users => {
    console.log("All users:", JSON.stringify(users, null, 4));
});
// Create a new user
User.create({ firstName: "Jane", lastName: "Doe" }).then(jane => {
    console.log("Jane's auto-generated ID:", jane.id);
});
// Delete everyone named "Jane"
User.destroy({
    where: {
        firstName: "Jane"
    }
}).then(() => {
    console.log("Done");
});
// Change everyone without a last name to "Doe"
User.update({ lastName: "Doe" }, {
    where: {
        lastName: null
    }
}).then(() => {
    console.log("Done");
});



```

快速查询 

```js

// controller层
const Student = require('../model/students')
 async getAdmin (ctx)  {
        const res = await Student.findAll()
        console.log(res)
          ctx.body = {
            data: res,
            statusCode: 200,
            message: '数据获取成功'
          }
   }


```

### 作业：建一个用户表（有姓名,年龄,性别,创建时间,修改时间）,使用模块加mysql模块,实现对数据的增删改查
###       修改时记得，把修改时间改成修改时的时间，能实现分页（选做）

### 作业：实现分页,能实现男女个数和总数的统计,能统计出（小于18） 和 18-45 和 45岁以上的人数（可以的话一条sql,分组,使用占位符的形式去写）



### 作业:第一个能不能使用promise的异步函数,封装下数据库的查询,二 把sequelize 配置完成,至少能查询一个表去看看sequelize的文档







