## 参数的获取

###  get参数使用rquest里面query获取

###  post需要额外的 body-parser

```js
  npm install body-parser


  //引入
  const bodyParser = require('body-parser');

  //实例去use
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));


```


## Express中间件

Express通过中间件接收客户端发来的请求，并对请求做出响应，也可以将请求交给下一个中间件继续处理。

Express中间件指业务流程中的中间处理环节，可以把中间件理解为客户端请求的一系列方法。如果把请求比作水流，那么中间件就是阀门，阀门可以控制水流是否继续向下流动，也可以在当前阀门处对水流进行排污处理，处理完成后再继续向下流动。

如图：

![image](./images/1.png)

#### 路由保护：当客户端访问登录页面时，可以先使用中间件判断用户的登录状态，如果用户未登录，则拦截请求，直接响应提示信息，并禁止用户跳转到登录页面。
#### 网站维护公告：在所有路由的最上面定义接收所有请求的中间件，直接为客户端做出响应，并提示网站正在维护中。
#### 自定义404页面：在所有路由的最上面定义接收所有请求的中间件，直接为客户端做出响应，并提示404页面错误信息。


常用的中间件方法有app.get()、app.post()、app.use()，其基本语法形式如下

```js
app.get('请求路径', '请求处理函数');  // 接收并处理GET请求
app.post('请求路径', '请求处理函数'); // 接收并处理POST请求
app.use('请求路径', '请求处理函数'); // 接收并处理所有请求

```


## app.get()中间件

当客户端向服务器端发送GET请求时，app.get()中间件方法会拦截GET请求，并通过app.get()中间件中的请求处理函数对GET请求进行处理。

```js
app.get('/', (req, res, next) => {
  next();
});



```
同一个请求路径可以设置多个中间件，表示对同一个路径的请求进行多次处理，默认情况下Express会从上到下依次匹配中间件。

示例：使用app.get()定义中间件并返回req.name的值

```js

// 引入express模块
const express = require("express");
// 创建Web服务器对象
const app = express();
// 定义中间件
app.get("/request", (req, res, next) => {
    req.name = "橘猫吃不胖";
    next(); // 启动下一个中间件
})
app.get("/request", (req, res) => {
    res.end(req.name);
})
// 监听3000端口
app.listen(3000);

```

## post 中间件

当浏览器向服务器发送POST请求时，app.post()定义的中间件会接收并处理POST请求。

示例：使用app.post()定义中间件发送post请求

第一步：新建一个用于发送post请求的表单index.htm

```js
    <form action="http://localhost:4000/post" method="post">
        <input type="submit" value="发送POST请求">
    </form>

```

第二步：定义app.post()中间件，接受并处理浏览器发送的POST请求，返回req.name的值

```js

const express = require("express");
const app = express();
// 定义中间件
app.post("/post", (req, res, next) => {
    req.name = "橘猫吃不胖";
    next();
})
app.post("/post", (req, res) => {
    res.end(req.name);
})
// 监听4000端口
app.listen(4000);

```

## app.use()中间件 

通过app.use()定义的中间件既可以处理GET请求又可以处理POST请求。在多个app.use()设置了相同请求路径的情况下，服务器都会接收请求并进行处理。

#### 全局中间件

```js
const express = require('express')
 
const app = express()
 
const mw = (req,res,next) => {
    console.log("你好")
    next()
}
 
app.use(mw)
 
app.get('/',(req,res) => {
    res.send('你好')
})
 
app.listen(80,() => {
    console.log("server at http://127.0.0.1")
})



```

### 指定中间件

```js

const mw = (req,res,next) => {
    console.log("你好")
    next()
}
app.post('/api',[mw],(req,res) => {
    
})

```

## 作业:上次作业实现分页,点击能实现数据的更新,最好能实现页数的展示,用中间件去记录后台接口，每次请求的url，方法,执行记录下来


