let express=require('express');
let fs=require('fs');
let app=express();

app.use(express.static('public'));

let zjmd=(req,res,next)=>{
    res.writeHead(200,{'Content-Type':'text/html;charset=utf8'})
    next();
}
app.use(zjmd);

//怎么实现404, 最尾部定义一个全局中间件
let notFoundMd = (req, res, next) => {
    fs.readFile("./public/html/404.html", (err, data) => {
        res.end(data);
        return;
    });
}

app.use(notFoundMd);
app.listen(8080);
