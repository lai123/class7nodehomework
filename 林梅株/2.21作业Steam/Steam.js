let fs=require("fs");
function big(){
    let str ="zcvbnmlikjghfdaqwddfrgthuykjkgjnfjdcg"
    let fd =fs.openSync("./big.txt","a");
    for(let i=0;i<1000000; i++){
        let start = Math.floor(Math.random()*(str.length));
        let end = Math.floor(Math.random()*(str.length));
        if(start > end){
            let tem =start;
            start = end;
            end =tem;
        }
        if(start==end) continue;

        let temstring =str.slice(start,end);

        fs.writeSync(fd,temstring);
    }
    fs.closeSync(fd);
}
let read=fs.createReadStream("./big.txt");
let array=[];
read.on("data",(chunk)=>{

    let str =chunk.toString();
    for(let i=0;i<str.length;i++){

        if(array[str[i]]==undefined){
            array[str[i]]=1;
        }else{
            array[str[i]] += 1;
        }
    }
});
read.on("end",()=>{
    console.log(array);
});

