let obj={
    add:function(x,y){
        return parseInt(x) + parseInt(y)
    },
    subtract:function(x,y){
        return parseInt(x) - parseInt(y)
    },
    multiply:function(x,y){
        return parseInt(x) * parseInt(y)
    },
    divide:function(x,y){
        return parseInt(x) / parseInt(y)
    }
}
module.exports=obj;