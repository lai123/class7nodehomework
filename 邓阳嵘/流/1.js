let fs = require("fs");
function createBigData() {
    let str = "qwertyuioopasdfghjklzxcvbnm";
    let fd = fs.openSync("./bigdata3.txt", "w");
    for (let i = 0; i < 1000000; i++) {
        let startAt = Math.floor(Math.random() * (str.length));
        let endAt = Math.floor(Math.random() * (str.length));
        if (startAt > endAt) {
            let tem = startAt;
            startAt = endAt;
            endAt = tem;
        }
        if (startAt == endAt) continue;

        let temStr = str.slice(startAt, endAt);
        fs.writeSync(fd, temStr);
    }
    fs.closeSync(fd);
}


let readStream = fs.createReadStream("./bigdata3.txt");
let array = [];
readStream.on("data", (chunk) => {
    let str = chunk.toString();
    for (let i = 0; i < str.length; i++) {
        if (array[str[i]] == undefined) {
            array[str[i]] = 1;
        } else {
            array[str[i]] += 1;
        }
    }
});

readStream.on("end",()=>{
    console.log(array);
})

