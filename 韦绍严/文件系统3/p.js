// 使用open write close的形式去创建一个大文件,内容从指定的文本中随机取一些（每次取得长度内容可以不一样）,计算出这个大文件出现最多的字符(要使用流的形式).

let fs = require('fs');

let bigFile = fs.openSync("./bigFile.txt", "w");
let content = "abcdefg";
let arr = [];
let x = Math.floor(Math.random() * 10)
for (let i = 0; i < x; i++) {
    let a = Math.floor(Math.random() * content.length)
    arr[i] = content[a];
    fs.writeSync(bigFile, "" + arr[i] + "");
}
fs.close(bigFile);

let rs = fs.createReadStream('./bigFile.txt', 'utf-8');

rs.on("data", function (chunk) {
    console.log("文件内容：" + chunk)
    var obj = {}
    for (let j = 0; j < chunk.length; j++) {
        var k = chunk.charAt(j);
        if (obj[k]) {
            obj[k]++;
        } else
            obj[k] = 1;
    }

    var max = 0;
    var result = '';
    for (var key in obj) {
        if (obj[key] > max) {
            max = obj[key];
            result = key;
        }
    }
    console.log("出现次数最多的字符：" + result);
})

rs.on("end", function () {
    console.log("----over----");
});

rs.on("error",function(err){
    console.log(err);
});