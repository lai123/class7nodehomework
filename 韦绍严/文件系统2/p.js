let fs = require("fs");

//同步
var start = new Date();
var startTime = start.getTime();
for (let i = 0; i < 100; i++) {
    let filename = "./file/" + i + ".txt";
    fs.writeFileSync(filename, "1");
}
var end = new Date();
var endTime = end.getTime();
console.log("同步耗时：" + (endTime - startTime) + "毫秒")

//异步
var start2 = new Date();
var startTime2 = start2.getTime();
var num = 0;
for (let j = 0; j < 100; j++) {
    let filename = "./file/" + j + ".txt";
    fs.writeFile(filename, "1", (error) => {
        num++;
        if (num == 100) {
            var end2 = new Date();
            var endTime2 = end2.getTime();
            console.log("异步耗时：" + (endTime2 - startTime2) + "毫秒")
        }
    });

}



