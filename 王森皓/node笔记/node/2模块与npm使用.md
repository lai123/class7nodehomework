## 什么是模块

在计算机程序的开发过程中，随着程序代码越写越多，在一个文件里代码就会越来越长，越来越不容易维护。

为了编写可维护的代码，我们把很多函数分组，分别放到不同的文件里，这样，每个文件包含的代码就相对较少，很多编程语言都采用这种组织代码的方式。在Node环境中，一个.js文件就称之为一个模块（module）。

使用模块有什么好处？

最大的好处是大大提高了代码的可维护性。其次，编写代码不必从零开始。当一个模块编写完毕，就可以被其他地方引用。我们在编写程序的时候，也经常引用其他模块，包括Node内置的模块和来自第三方的模块。

使用模块还可以避免函数名和变量名冲突。相同名字的函数和变量完全可以分别存在不同的模块中，因此，我们自己在编写模块时，不必考虑名字会与其他模块冲突。


#### 简单来说，模块就是把代码分成一个个文件


#### 来个案例

```js
//文件 a

'use strict';

var s = 'Hello';

function greet(name) {
    console.log(s + ', ' + name + '!');
}

module.exports = greet;

//文件 b


'use strict';

// 引入hello模块:
var greet = require('./a');

var s = 'Michael';

greet(s); // Hello, Michael!

```

#### 简单说要暴露的使用 module.exports 暴露， 要使用的 require（跟php 类似）引入文件，要注意文件的位置


## 什么是 npm

npm是什么东东？npm其实是Node.js的包管理工具（node package manager）。

为啥我们需要一个包管理工具呢？因为我们在Node.js上开发时，会用到很多别人写的JavaScript代码。如果我们要使用别人写的某个包，每次都根据名称搜索一下官方网站，下载代码，解压，再使用，非常繁琐。于是一个集中管理的工具应运而生：大家都把自己开发的模块打包后放到npm官网上，如果要使用，直接通过npm安装就可以直接用，不用管代码存在哪，应该从哪下载。

更重要的是，如果我们要使用模块A，而模块A又依赖于模块B，模块B又依赖于模块X和模块Y，npm可以根据依赖关系，把所有依赖的包都下载下来并管理起来。否则，靠我们自己手动管理，肯定既麻烦又容易出错。


## npm的常用命令

2.npm 常用命令 参考 https://www.npmjs.cn/cli/init/

npm install //安装 也可简写成 npm i 如何指定版本gulp@3.9.1

npm uninstall //移除

npm update //更新 npm + 英文

npm ls //列出列表

npm init //初始化 init 初始化

npm cache

npm start

npm outdated 检查模块是否已经过时

npm root 查看包的安装路径

npm version 查看模块版本

npm install -g cnpm --registry=https://registry.npm.taobao.org

npm 的官网 http://npmjs.com/

3.package.json 介绍 name 包名 description 包简介 author 作者 version 版本 repository 源码托管地址 maintainers 包维护者列表 cotributors 贡献者列表 dependencies 需要的依赖包列表 devde-- 开发环境需要的包列表 keywords 关键词数组 main 模块的入口文件 scripts 脚本说明对象

### 作业：写个加减乘除的模块（尽量少写暴露,要考虑复用性），供外部使用. 作业每个分支要建一个目录（自己名字）








