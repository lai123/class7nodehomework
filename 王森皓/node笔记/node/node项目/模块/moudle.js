

function giveFlower() {
    console.log('送花');
    return "送花";
}

function giveGift() {
    console.log("手机");
    return "苹果手机";
}

function giveMoney() {
    console.log("money");
    return "给钱";
}

function giveBook() {
    console.log("一起好好读书");
    return "一起自习";
}

var money = 200;
function changeMoney(num){
    money = money+num;
    console.log(money);

}

// changeMoney(500);
// console.log(money);


//module.exports 理解为一个对外暴露的空对象
// let obj={}
// obj.name = function(){

// }
module.exports.flower = giveFlower;
module.exports.giveMoney = giveMoney;
module.exports.giveBook = giveBook;
module.exports.qian = money;//等效于 module.exports.qian = 200
module.exports.changeMoney = changeMoney;


