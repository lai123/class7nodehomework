let obj ={
    js:function(){
        return 4000;
    },
    'php':function(){
        return 4000;
    },
    python:function(){
        return 5000;
    },
    money:300,
}



//module.exports 可以理解对外暴露的空对象
//module.exports.obj = obj;//也可以只是使用的模块,多了一层obj
module.exports = obj;