//全局变量
//__filename 获取文件的路径，获取到绝对路径，包括文件名
//__filename 前面是两个下划线
console.log(__filename);
console.log('__filename');
//eval 会把字符串当做js代码执行
console.log(eval('__filename'));

//__dirname  获取当前文件所在的文件夹
//__dirname 前面也是两个下划线
console.log(__dirname);

//过一段时间执行里面的方法，只执行一次，第二次参数 timeout是 时间，单位毫秒
setTimeout(() => {
    console.log("过三秒后执行")
}, 3000);
console.log("我会不会先执行");

//周期性执行
// setInterval(() => {
//     console.log("看能不能周期性的执行")
// }, 2000);

//process 当前的这个程序
//console.log(process);

process.on('exit',()=>{
    setTimeout(() => {
        console.log('-----------------------------------------------------我会不会被执行')
    }, 0);
    console.log('要退出了')
});

process.on('beforeExit',()=>{
    console.log("我能不能优先于上面的代码先输出");
})




