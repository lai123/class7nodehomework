## 静态资源的处理和中间件的应用

express.static()是Express框架提供的内置中间件，它接收静态资源访问目录作为参数。

使用express.static()内置中间件可以方便地托管静态文件，在客户端访问服务器的静态资源时使用。常用的静态资源有图片、CSS、JavaScript和HTML文件等。

express.static()参数是静态资源所在的目录，它需要作为app.use()的参数使用，示例代码如下

```js
app.use(express.static('public'));

```

举个栗子：实现静态资源访问

第一步：在src目录下新建public文件夹，用于存放静态文件，在该目录下新建一个文件夹images，存放一个图片文件


第二步：在src目录下新建static.js文件，代码如下
```js
const express = require("express");
const app = express();
// 静态资源处理
app.use(express.static("public"));
// 监听3000端口
app.listen(3000);

```
第三步：启动服务器，在浏览器中输入：localhost:3000/images/1.jpg，页面上就会显示出来放在images文件夹下面的图片

## 利用中间件处理错误

在程序执行的过程中，不可避免的会出现一些无法预料的错误，比如文件读取失败、数据库连接失败等。这时候就需要用到错误处理中间件了，用它来集中处理错误。

利用app.use()定义错误处理中间件的示例代码如下：

```js
app.use((err, req, res, next) => {
  console.log(err.message);
});


const express = require("express");
const fs = require("fs");
const app = express();
// 使用app.get()中间件进行文件读取操作
app.get("/readFile", (req, res, next) => {
    // 读取a.txt文件
    fs.readFile("./a.txt", "utf8", (err, result) => {
        if (err !== null) { // 如果错误信息不为空，将该信息传给下一个中间件
            next(err);
        } else {
            res.send(result);
        }
    })
})
// 错误处理中间件
app.use((err, req, res, next) => {
    // 设置响应状态码为500，并发送错误信息
    res.status(500).send(err.message);
})
app.listen(3000);


```