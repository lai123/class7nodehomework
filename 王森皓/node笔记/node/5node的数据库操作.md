## 对mysql的操作

当我们安装好MySQL后，Node.js程序如何访问MySQL数据库呢？

访问MySQL数据库只有一种方法，就是通过网络发送SQL命令，然后，MySQL服务器执行后返回结果。

我们可以在命令行窗口输入mysql -u root -p，然后输入root口令后，就连接到了MySQL服务器。因为没有指定--host参数，所以我们连接到的是localhost，也就是本机的MySQL服务器。

在命令行窗口下，我们可以输入命令，操作MySQL服务器：

## 对mysql的直接操作

如果直接使用mysql包提供的接口，我们编写的代码就比较底层，例如，查询代码

```js

connection.query('SELECT * FROM users WHERE id = ?', ['123'], function(err, rows) {
    if (err) {
        // error
    } else {
        for (let row in rows) {
            processRow(row);
        }
    }
});
```

## 传说中的orm

这就是传说中的ORM技术：Object-Relational Mapping，把关系数据库的表结构映射到对象上。是不是很简单？

但是由谁来做这个转换呢？所以ORM框架应运而生。

我们选择Node的ORM框架Sequelize来操作数据库。这样，我们读写的都是JavaScript对象，Sequelize帮我们把对象变成数据库中的行。

用Sequelize查询pets表，代码像这样：

```js

Pet.findAll()
   .then(function (pets) {
       for (let pet in pets) {
           console.log(`${pet.id}: ${pet.name}`);
       }
   }).catch(function (err) {
       // error
   });

   ```

## ORM是什么？
ORM（Object Relational Mapping，对象关系映射），是一种为了解决面向对象与关系数据库存在的互不匹配的现象的技术，通过描述对象和数据库之间映射的元数据，把程序中的对象自动持久化到关系数据库中。它的作用是在关系型数据库和对象之间作一个映射，这样，我们在具体的操作数据库的时候，就不需要再去和复杂的SQL语句打交道，只要像平时操作对象一样操作它就可以了 。这就好比入java的Mybatis， thinkphp的model类，通过映射数据库，简化数据库操作，使开发者不需要书写复杂的SQL语句，将更多的时间放在逻辑的书写上。



sequelize的基本使用

下载

```js
npm install --save sequelize

```

安装数据库驱动程序：

```js

$ npm install --save pg pg-hstore # Postgres
$ npm install --save mysql2
$ npm install --save mariadb
$ npm install --save sqlite3
$ npm install --save tedious # Microsoft SQL Server


```

创建数据库配置

```js

const { Sequelize } = require('sequelize');

// 方法 1: 传递一个连接 URI
const sequelize = new Sequelize('sqlite::memory:') // Sqlite 示例
const sequelize = new Sequelize('postgres://user:pass@example.com:5432/dbname') // Postgres 示例

// 方法 2: 分别传递参数 (sqlite)
const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: 'path/to/database.sqlite'
});

// 方法 3: 分别传递参数 (其它数据库)
const sequelize = new Sequelize('database', 'username', 'password', {
  host: 'localhost',
  dialect: /* 选择 'mysql' | 'mariadb' | 'postgres' | 'mssql' 其一 */
});


```
连接测试

```js
使用 .authenticate() 函数测试连接是否正常
 try {
  await sequelize.authenticate();
  console.log('Connection has been established successfully.');
} catch (error) {
  console.error('Unable to connect to the database:', error);
}

```

同步数据库

```js
sequelize.sync() - 如果不存在,则创建该表(如果已经存在,则不执行任何操作)
sequelize.sync({ force: true }) - 将创建表,如果表已经存在,则将其首先删除
sequelize.sync({ alter: true }) - 这将检查数据库中表的当前状态(它具有哪些列,它们的数据类型等)
```

实例

```js

const sequelize = new Sequelize('admin', 'root', '893100', {
    host: 'localhost',
    port: 3306,
    dialect: 'mysql'
  });

  try {
    sequelize.authenticate();
    // console.log('Connection has been established successfully.');
  } catch (error) {
    // console.error('Unable to connect to the database:', error);
  }

sequelize.sync({ alter: true });

  module.exports = sequelize

```

创建数据库模型

```js

const { DataTypes } = require('sequelize');
const sequelize = require('../config/db')

const Student = sequelize.define('student', {
    // 这里定义模型属性
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    sex: {
      type: DataTypes.STRING
      // allowNull 默认为 true
    },
    QQ: {
        type: DataTypes.STRING
        // allowNull 默认为 true
      },
    id: {
        type: DataTypes.STRING,
        primaryKey: true
        // allowNull 默认为 true
      },
    number: {
        type: DataTypes.STRING
        // allowNull 默认为 true
    },
    telphone: {
        type: DataTypes.STRING
        // allowNull 默认为 true
    },
    classe: {
        type: DataTypes.STRING
        // allowNull 默认为 true
    }
  }, {
    freezeTableName: true
  });


  module.exports = Student

```

快速查询 

```js

// controller层
const Student = require('../model/students')
 async getAdmin (ctx)  {
        const res = await Student.findAll()
        console.log(res)
          ctx.body = {
            data: res,
            statusCode: 200,
            message: '数据获取成功'
          }
   }


```

## 作业：创建一个用户表（要有主键，用户名，密码，创建时间，更新时间，性别）等，使用orm框架去数据库进行增删改查





