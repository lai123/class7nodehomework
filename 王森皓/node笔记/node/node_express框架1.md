## node 的框架 

### 初始Express

1.1 Express介绍
Express是目前流行的基于Node.js运行环境的Web应用程序开发框架，它简洁且灵活，为Web应用程序提供了强大的功能。Express提供了一个轻量级模块，类似于jQuery（封装的工具库），它把Node.js的HTTP模块的功能封装在一个简单易用的接口中，用于扩展HTTP模块的功能，能够轻松地处理服务器的路由、响应、Cookie和HTTP请求的状态。

Express的优势：
（1）简洁的路由定义方式。
（2）简化HTTP请求参数的处理。
（3）提供中间件机制控制HTTP请求。
（4）拥有大量第三方中间件。
（5）支持多种模版引擎

### 安装express 框架

```js

// 项目初始化
npm init -y
// 安装
npm install express --save

// 查看版本

npm list express


```

##  利用Express搭建Web服务器

利用Express搭建Web服务器的基本步骤：

#### 引入express模块；
#### 调用express()方法创建服务器对象；
#### 定义路由；
#### 调用listen()方法监听端口


来个例子

```js

// 引入express模块
const express = require("express");
// 创建Web服务器对象
const app = express();
// 定义GET路由，接收/处理客户端的GET请求
app.get("/", (req, res) => {
    // 对客户端做出响应，send()方法会根据内容的类型自动设置请求头
    res.end("hello express");
})
// 监听3000端口
app.listen(3000);

```
#### 理解 app 是啥，req，res 是啥.怎么接收参数,怎么回应参数.

## 作业：写个个人简历



