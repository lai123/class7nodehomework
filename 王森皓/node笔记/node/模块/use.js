//使用模块,需要使用一个关键词 require
let obj=require("./moudle.js");
console.log(obj);// 引入一个模块就是一个对象

obj.flower();

//require进来,.js可以省去, 如果是同一层级 ./ 不能省去
let useObj = require("./moduleObj");
// console.log(useObj);
// console.log(useObj.obj.js())
console.log(useObj.js());

//修改module.js里面的金额
console.log(obj.qian);//
obj.changeMoney(500);
console.log(obj.qian);//200 还是 700
