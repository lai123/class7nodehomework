## 参数的接收

### 接受GET请求

Express框架中的req.query用于获取GET请求参数，框架内部会将GET参数转换为对象并返回。利用req.query获取GET请求参数的示例代码如下。

```js

app.get('/', (req, res) => {
  res.send(req.query);
});


```


示例：获取GET请求参数

```js
const express = require("express");
const app = express();
app.get("/query", (req, res) => {
    // 获取get请求参数
    res.send(req.query);
})
app.listen(3000);

```



## 接收POST请求参数
Express中的req.body用于获取POST请求参数，需要借助第三方body-parser模块将POST参数转换为对象形式。利用req获取POST请求参数的示例代码如下。

示例：

### 第一步：安装body-parser模块
```js
npm install body-parser@1.18.3 --save

```
### 第二步：编写query.js文件

```js

const express = require("express");
const bodyParsee = require("body-parser");
const app = express();
app.use(bodyParsee.urlencoded({ extended: false }));
app.post("/query", (req, res) => {
    // 获取get请求参数
    res.send(req.body);
})
app.listen(3000);


```

### 第三步：编写index.html文件


```js

    <form action="http://localhost:3000/query" method="post">
        用户名：<input type="text" name="username">
        <br>
        密码：<input type="password" name="password">
        <br>
        <input type="submit" value="提交">
    </form>


```

