//作业：用同步异步分别生成1000个文件，分别同步的耗时异步的耗时 
let fs = require("fs");
//如果文件不存在就创建同步文件
if (!fs.existsSync("homework")) fs.mkdirSync("homework");
//计算同步创建1000个文件的耗时 writeFileSync表示写入文件同步 nr代表内容可以随便写
//1.先写出开始的时间戳 再入同步完文件后 计算结束时间戳 相减就耗时
let homestartime = new Date().getTime();
for (let i = 0; i < 1000; i++) {
    fs.writeFileSync("./homework/" + i + ".text", "nr")
}
let homeendtime = new Date().getTime();
console.log("同步创建时间为" + (homeendtime - homestartime));

//异步创建文件
let num=0;
//如果文件不存在就创建同步文件
if (!fs.existsSync("ahomework")) fs.mkdirSync("ahomework");
let ahomestartime = new Date().getTime();
//err 函数在异步创建是固定的 就是接受错误信息
for (let j = 0; j < 1000; j++) {
     fs.writeFile("./ahomework/" + j + ".text", "nr",(err)=>{
          num++;
          if(num==1000){
            let ahomeendtime = new Date().getTime();
            console.log("异步创建的时间:" + (ahomeendtime  - ahomestartime));
          }
     })
    
}