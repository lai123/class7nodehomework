let fs = require("fs");
// 生成1000个文件
for (let i = 0; i < 1000; i++) {
    let filename = "./files/" + i + ".txt";
    fs.writeFileSync(filename, Math.floor(Math.random() * 1001) + "");
}
// 标记开始时间
console.time("异步的读取的时间");
//异步的读取
for (let i = 0; i < 1000; i++)
        {
        fs.writeFileSync("./files/"+i+".txt",""+i)         
        } 
// 标记结束时间
console.timeEnd("异步的读取的时间");

console.time("同步的读取的时间");

//同步的读取
for (let i = 0; i < 1000; i++)
        {
        fs.writeFileSync("./files/"+i+".txt",""+i)         
        } 
console.timeEnd("同步的读取的时间");