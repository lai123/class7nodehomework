// 作业2:生成1000个文件，用同步异步分别计算生成文件的时间.
let fs = require("fs");

// 同步写入
//方法一：
var T1=new Date().getTime();
for(let i=0;i<1000;i++){
    var nowDate = new Date();
    var fileName = "./files/"+i;
    fs.writeFileSync(fileName,"写入文件的时间戳为:"+nowDate.getTime()+"ms,写入文件的时间为:"+nowDate.toLocaleString());
}
var T2=new Date().getTime();
console.log("方法一同步创建1000个文件消耗的时间为："+(T2-T1)+"ms");

// 方法二：同步写入异步读取（等待两秒后读取）
for(let i=0;i<1000;i++){
    var nowDate = new Date();
    var fileName = "./files/"+i;
    fs.writeFileSync(fileName,"写入文件的时间戳为:"+nowDate.getTime()+"ms,写入文件的时间为:"+nowDate.toLocaleString());
}
var startTime = null ;
var endTime = null;
fs.readFile("./files/0",(error,data)=>{
    startTime = data.toString().substring(10,23);
    console.log("同步文件写入的开始时间戳为:"+startTime);
});
fs.readFile("./files/999",(error,data)=>{
    endTime = data.toString().substring(10,23);
    console.log("同步文件写入的结束时间戳为:"+endTime);
});
setTimeout(() => {
    console.log("方法二同步创建1000个文件消耗的的时间为:"+(endTime-startTime)+"ms")
}, 2000);

// 异步写入
setTimeout(() => {
    var t1=new Date().getTime();
    for(let j=0;j<1000;j++){
        var nowDate = new Date();
        var fileName = "./files/D"+j;
        fs.writeFile(fileName,"写入文件的时间戳为:"+nowDate.getTime()+"ms,写入文件的时间为:"+nowDate.toLocaleString(),(error,data)=>{});
    }
    var t2=new Date().getTime();
    console.log("异步创建1000个文件消耗的时间为："+(t2-t1)+"ms");
}, 2500);