//文件读取需要使用fs模块,fs node自带的
let fs = require("fs");
//异步读取文件
// fs.readFile('./weekend.txt', (error, data) => {
//     console.log(error);
//     console.log(data);//data 是 Buffer 类型
//     console.log(data.toString());//1
// });
// console.log("我在前面还是后面输出");//

//同步读取文件
// let data = fs.readFileSync("./sports.txt");
// console.log(data.toString());
// console.log("会不会先输出");

//假设读取文件 100毫秒，一个函数执行5毫秒，异步方式执行这个代码，总的耗时是多少 100毫秒，同步方式 105毫秒

//文件写入

//同步文件写入,有这个Sync就是同步

// 作业：生成100个文件，每个文件存入一个1到1000的随机数字，再取出最大值的那个文件,值也要取出来,再取出最小的那个.
var arr=[];
for(let i=1;i<=10;i++){
    var a = Math.floor(Math.random()*1001);
    fs.writeFileSync(i,""+a);
    arr.push(a);
    fs.readFile(+i,(error,data)=>{
        var num = data.toString();
        console.log("该文件夹后缀为:"+i+",数值为："+num);
    })
}
var max=arr[0];
var maxhz=1;
for(let ii=0;ii<arr.length;ii++){
    if(arr[ii]>max){
        max=arr[ii];
        maxhz=ii+1;
    }
}

var min=arr[0];
var minhz=1;
for(let iii=0;iii<arr.length;iii++){
    if(arr[iii]<min){
        min=arr[iii]
        minhz=iii+1;
    }
}
console.log("---最大值为:"+max+",该文件名为：D"+maxhz);
console.log("---最小值为:"+min+",该文件名为：D"+minhz);
