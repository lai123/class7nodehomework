//作业：创建一个学生表,里面有班级字段,里面最好有自己班上同学姓名,其它班级的姓名随机写,有个字段存储游戏的分值,先实现随机的数值写入,查询出各班级前三名的学生
// tips：先要用npm 下载 sequelize和mysql2
let con=require("./config");
const { Sequelize, Model, DataTypes } = require('sequelize');
const sequelize = new Sequelize(con.database, con.user, con.password, {
    //申明下使用的数据库
    dialect: 'mysql',
    host:con.host,//如果变量的key和value的变量一致，可以简写
    port: con.port,
    logging: true,//日志
    timezone: '+08:00',//东八区
    define: {
        timestamps: false,//默认会开启创建时间和修改时间
        underscored: true,//会把对象的属性名,由小驼峰转成下划线
        deletedAt:false,
    }
});

class Student extends Model { }

Student.init({
    // attributes
    stu_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement:true,
    },
    stu_class: {
        type: DataTypes.STRING
        // allowNull defaults to true
    },
    stu_name: {
        type: DataTypes.STRING
    },
    score: {
        type: DataTypes.BIGINT
    },
    
}, {
    sequelize,
    modelName: 'stu_game',
    // options
    tableName: 'stu_game',//要关联表
});


//update
//  async function update(){
//     await Student.update({
//         score: 45
//     },{
//         where:{score: null}
//     })
//  } 
//  update()

  function getscoremax3(){
    let all= Student.findAll({
    order: [['score','DESC']],
    limit:3

 })
   all.then((all)=>{
    console.log(JSON.stringify(all,null,4) );
   })
}
getscoremax3()
