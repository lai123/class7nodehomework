/*
 Navicat Premium Data Transfer

 Source Server         : Student
 Source Server Type    : MySQL
 Source Server Version : 50737
 Source Host           : localhost:3306
 Source Schema         : student

 Target Server Type    : MySQL
 Target Server Version : 50737
 File Encoding         : 65001

 Date: 27/02/2023 17:40:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for stu_game
-- ----------------------------
DROP TABLE IF EXISTS `stu_game`;
CREATE TABLE `stu_game`  (
  `stu_id` int(11) NOT NULL AUTO_INCREMENT,
  `stu_class` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `stu_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `score` bigint(20) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`stu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of stu_game
-- ----------------------------
INSERT INTO `stu_game` VALUES (1, '7班', 'wjj', NULL);
INSERT INTO `stu_game` VALUES (2, '7班', 'zm', NULL);
INSERT INTO `stu_game` VALUES (3, '2班', 'wwl', NULL);

SET FOREIGN_KEY_CHECKS = 1;
