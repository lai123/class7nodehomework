
//写个加减乘除的模块（尽量少写暴露,要考虑复用性），供外部使用. 作业每个分支要建一个目录（自己名字）

module.exports = {
    add: function (a, b) {
        if (this.error(a, b)) {
            return a + b;
        } else {
            return "输入参数类型不是number"
        }
    },
    subtract: function (a, b) {
        this.error(a, b);
        return a - b;
    },
    multiply: function (a, b) {
        return a * b;
    },
    divide: function (a, b) {
        if (this.error(a, b)) {
            if (b == 0) {
                return "被除数为0！"
            }
            return a / b;
        } else {
            return "输入参数类型不是number"
        }
    },
    error: (a, b) => {
        if (isNaN(a) == true || isNaN(b) == true) {
            return false;
        } else {
            return true;
        }
    }
}
