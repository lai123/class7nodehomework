let conn = require("./dbmodule");
//引入
const { Sequelize, Model, DataTypes } = require('sequelize');

//初始化对象
const sequelize = new Sequelize(conn.database, conn.user, conn.password, {
    //申明下使用的数据库
    dialect: 'mysql',
    host: conn.host,
    port: conn.port,
    logging: true,//日志
    timezone: '+08:00',//东八区
    define: {
    timestamps: false,
    }
});

//要不要创建表
sequelize.sync({ force: false })

//创建对象,构建模型
class Dis extends Model { }

Dis.init({
    // attributes
    dis_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    dis_user: {
        type: DataTypes.STRING
        // allowNull defaults to true
    },
    dis_divide: {
        type: DataTypes.STRING
    }
    
}, {
    sequelize,
    modelName: 'dis',
    // options
    tableName: 'dis',//要关联表
});

Dis.findAll().then((data)=>{
    console.log(JSON.stringify(data, null, 4));
})