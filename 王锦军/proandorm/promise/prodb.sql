/*
 Navicat Premium Data Transfer

 Source Server         : test
 Source Server Type    : MySQL
 Source Server Version : 50737
 Source Host           : localhost:3306
 Source Schema         : prodb

 Target Server Type    : MySQL
 Target Server Version : 50737
 File Encoding         : 65001

 Date: 25/02/2023 11:00:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dis
-- ----------------------------
DROP TABLE IF EXISTS `dis`;
CREATE TABLE `dis`  (
  `dis_id` int(11) NOT NULL AUTO_INCREMENT,
  `dis_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dis_divide` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`dis_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dis
-- ----------------------------
INSERT INTO `dis` VALUES (1, '程晟', '二辩');
INSERT INTO `dis` VALUES (2, 'wjj', '四辩');
INSERT INTO `dis` VALUES (3, '行健', '三辩');

SET FOREIGN_KEY_CHECKS = 1;
