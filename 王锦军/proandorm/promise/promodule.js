// 作业:第一个能不能使用promise的异步函数,封装下数据库的查询,
let mysql = require("mysql");
let conn = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "prodb"
})

const { rootCertificates } = require("tls");
module.exports = {
    read: function (readsql) {
        return new Promise((resolve, reject) => {
            conn.connect((error) => {
                if (error) {
                    throw error;
                }
            })

            conn.query(readsql, (error, data) => {
                resolve(data);
                reject(error);
            })

        })
    },
    dbend: function () {
        conn.end()
    }
}