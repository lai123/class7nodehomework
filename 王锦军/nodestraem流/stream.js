//作业1:使用open write close的形式去创建一个大文件,内容从指定的文本中随机取一些（每次取得长度内容可以不一样）,计算出这个大文件出现最多的字符(要使用流的形式).
let fs = require("fs");
let fd = fs.openSync("./bigfile/", "a");
let str = "朱章彬是骚杯彬彬有礼彬彬就是逊了彬彬天天实名观看美女彬彬不流氓发育不正常彬彬不发骚太阳都升不高!";

for (let i = 0; i < 100000; i++) {
    let starnum = Math.floor(Math.random() * str.length);
    let radlength = Math.ceil(Math.random() * str.length);
    let content = str.substr(starnum, radlength);
    fs.writeSync(fd, content);
}
console.log(fs.readFileSync("./bigfile/").toString());
console.log(maxstr(fs.readFileSync("./bigfile/").toString()));
fs.closeSync(fd);


//计算出大文件内容里出现最多的字符的方法
function maxstr(content) {
    let obj = {}
    for (let j = 0; j < content.length; j++) {
        if (obj[content[j]] == undefined) {
            obj[content[j]] = 1;
        } else {
            obj[content[j]]++;
        }
    }

    let maxstring = null;
    let max = -Infinity;
    for (let key in obj) {
        if (obj[key] > max) {
            max = obj[key];
            maxstring = key;
        }
    }
    return maxstring;
}


//https://blog.csdn.net/w1099690237/article/details/119192820