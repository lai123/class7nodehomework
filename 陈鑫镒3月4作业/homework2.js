// ### 作业：创建一个学生表,里面有班级字段,里面最好有自己班上同学姓名,其它班级的姓名随机写,有个字段存储游戏的分值,先实现随机的数值写入,查询出各班级前三名的学生

let config=require("./config");
const { Sequelize , Model,DataTypes}=require("sequelize");
let host =config.host;
const sequelize = new Sequelize(config.database, config.user, config.password, {
    //申明下使用的数据库
    dialect: 'mysql',
    host:config.host,
    port: config.port,
    logging: true,
    timezone: '+08:00',
    define: {
        timestamps: false,
        
    }
});
sequelize.sync({force:false})
class user extends Model{}
user.init({
    id:{
        type:DataTypes.INTEGER,
        allowNull:false,
        primaryKey: true,
        autoIncrement:true
    },
    name:{
        type: DataTypes.STRING
    },
    class:{
        type: DataTypes.INTEGER
    },
    score:{
        type: DataTypes.INTEGER
    }

},{
    sequelize,
    modelName:'user',
    tableName:'user',
});
user.findAll().then(name =>{
    //console.log(JSON.stringify(name,null,2));
})


// setTimeout(function(){

//     let user=user.build({name:'msk',class:6,score:60});
//     user.save();
    
// },1000);
function add(){
   
    let auser=user.build({name:'cxy',class:'6',score:Math.floor(Math.random() * 101)});
    let auser2=user.build({name:'lsz',class:'7',score:Math.floor(Math.random() * 101)});
    let auser3=user.build({name:'zzb',class:'7',score:Math.floor(Math.random() * 101)});
    auser.save();
    auser2.save();
    auser3.save();
};

// for(i=1;i<=5;i++){
//     add();
// }
user.findAll({
    where: {    
    class:7,
    },
    limit:3,
    order:[
        ["score", "DESC"]
    ]
}).then(users => {
    console.log("正确的输入:", JSON.stringify(users, null, 4));
});