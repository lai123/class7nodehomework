//### 作业:第一个能不能使用promise的异步函数,封装下数据库的查询,二 把sequelize 配置完成,至少能查询一个表去看看sequelize的文档,实现curd



let mysql=require("mysql2");
let conn=mysql.createConnection({ host:"localhost", user: "root", password: "root", port: "3306", database: "user"})
conn.commit();


async function useasync(){
    let data=await use();
    // console.log(data);
}

function use(){
    return new Promise((resolve,rejects)=>{
        let select="select*from student ";
        conn.query(select,(err,data)=>{
            console.log(err);
            console.log(data);
            if(err!=null){
                rejects(err);
            }else{
                resolve(data);
            }
        })
    })
}
useasync();
conn.end();
